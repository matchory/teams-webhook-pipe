# Bitbucket Pipelines Pipe: Teams Webhook Pipe
A Pipe for BitBucket Pipelines allowing to send messages to [Microsoft Teams Incoming Webhooks][Incoming Webhook].

## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: matchory/teams-webhook-pipe:0.1.4
  variables:
  WEBHOOK_URL: '<string>'
  MESSAGE: '<string>'
  # PAYLOAD_FILE: '<string>' # Optional.
  # DEBUG: '<boolean>' # Optional.
```

## Variables
| Variable        | Usage                                                                                                                                                |
| --------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| WEBHOOK_URL (*) | Incoming Webhook URL. It is recommended to use a secure repository variable.                                                                         |
| MESSAGE (*)     | Notification attachment message.                                                                                                                     |
| PAYLOAD_FILE    | Path to JSON file containing custom payload. Build your own with [the Adaptive Cards Designer][Adaptive Cards Designer]. Default: `''(empty string)` |
| DEBUG           | Turn on extra debug information. Default: `false`.                                                                                                   |

_(*) = required variable._

## Prerequisites
To send notifications to Microsoft Teams, you need an Incoming Webhook URL. You can follow the instructions [here][Incoming Webhook] to create one.

## Examples
**Basic example:**  

```yaml
script:
  - pipe: matchory/teams-webhook-pipe:0.1.4
    variables:
      WEBHOOK_URL: $WEBHOOK_URL
      MESSAGE: 'Hello, world!'
```

**Advanced example:**  
If you want to pass complex string with structure elements, use double quotes

```yaml
script:
  - pipe: matchory/teams-webhook-pipe:0.1.4
    variables:
      WEBHOOK_URL: $WEBHOOK_URL
      MESSAGE: '"[${ENVIRONMENT_NAME}] build has exited with status $build_status"'
```

Use custom payload created with [the Adaptive Cards Designer][Adaptive Cards Designer] and modify payload with the [envsubst][envsubst] program that substitutes the values of environment variables:

```yaml
script:
  - envsubst < "payload.json.template" > "payload.json"
  - pipe: matchory/teams-webhook-pipe:0.1.4
    variables:
      WEBHOOK_URL: $WEBHOOK_URL
      PAYLOAD_FILE: payload.json
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by moritz@matchory.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[Incoming Webhook]: https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook
[Adaptive Cards Designer]: https://adaptivecards.io/designer
[envsubst]: https://www.gnu.org/software/gettext/manual/html_node/envsubst-Invocation.html

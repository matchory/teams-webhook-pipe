import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class TeamsotifyTestCase(PipeTestCase):

    def test_default_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": "Pipelines is awesome!"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_double_quotes_in_message_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": '"Pipelines is awesome!"'
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_debug_flag_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": "Pipelines is awesome!",
            "DEBUG": "true"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_default_failed(self):
        BAD_WEBHOOK_URL = "https://example.webhook.office.com/webhookb2/00000000-0000-0000-0000-000000000000@00000000-0000-0000-0000-000000000000/IncomingWebhook/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/00000000-0000-0000-0000-000000000000"

        result = self.run_container(environment={
            "WEBHOOK_URL": BAD_WEBHOOK_URL,
            "MESSAGE": "Pipelines is awesome!"
        })

        self.assertRegex(result, rf'✖ Notification failed.')

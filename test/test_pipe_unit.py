import io
import os
import sys
from unittest import mock
from contextlib import contextmanager
from bitbucket_pipes_toolkit.test import PipeTestCase
from pipe.pipe import TeamsNotifyPipe, schema

TEST_WEBHOOK_URL = "https://example.webhook.office.com/webhookb2/00000000-0000-0000-0000-000000000000@00000000-0000-0000-0000-000000000000/IncomingWebhook/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/00000000-0000-0000-0000-000000000000"


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


@mock.patch('requests.post')
@mock.patch.dict(os.environ, {'WEBHOOK_URL': TEST_WEBHOOK_URL, 'MESSAGE': 'Hello!'})
class TeamsNotifyTestCase(PipeTestCase):

    def test_notify_succeeded(self, teams_status_mock):

        teams_status_mock.return_value.status_code = 200
        teams_status_mock.return_value.text.return_value = 'ok'

        with capture_output() as out:
            TeamsNotifyPipe(schema=schema, check_for_newer_version=True).run()

        self.assertRegex(out.getvalue(), rf"✔ Notification successful.")

    def test_notify_failed(self, teams_status_mock):

        teams_status_mock.return_value.status_code = 400
        teams_status_mock.return_value.text.return_value = 'invalid_token'

        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                TeamsNotifyPipe(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(out.getvalue(), rf"✖ Notification failed.")
